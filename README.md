# line-scan-summer.py #

A python (v2.7) script for calculating the area under the curve in line scans.

This works by using the average of the first and last 5 peaks as background, then
calculating total area by summing all of the peaks in the line scan - that background.

The results are output to the current directory as line_scan_sums.txt which can be
imported into Excel as a tab delimited document.

See example_input.txt for an example of the input the script expects. The data must
be strictly formatted in this way.
