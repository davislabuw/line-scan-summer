# read and print some summary line scan intensity statistics
# for each line scan, print the: label, count (number of intensities),
# calculated background, and sum of the intensities (each minus the
# background)
#
# Michael Riffle <mriffle@uw.edu>
# October 2018

import sys
import os.path

# where we're storing the data
the_data = { }

# methods

def get_data_labels( line ):
    line = line.rstrip()
    fields = line.split( "\t" )
    column_headers = [ ]

    for i, field in enumerate( fields ):
        if i % 2 == 0:
            if field != '':
                column_headers.append( field )
    
    return column_headers

def initialize_the_data( data_labels ):

    for column in data_labels:
        the_data[ column ] = [ ]


def add_line_to_the_data( line, data_labels ):
    line = line.rstrip()

    if line == '':
        return

    fields = line.split( "\t" )

    for i, data_label in enumerate( data_labels ):

        data_column_index = i * 2 + 1

        if data_column_index > len( fields ) - 1:
            continue

        string_float = fields[ data_column_index ]

        if string_float == '':
            continue

        the_data[ data_label ].append( float( string_float ) )


# read data from the data file into the 
# 'the_data' global variable
def read_in_the_data( filename ):
    line_scan_file = open( filename, 'r' )

    for i, line in enumerate(line_scan_file):
        if i == 0 or i == 2:
            continue
        
        if i == 1:
            data_labels = get_data_labels( line )
            initialize_the_data( data_labels )
            continue

        add_line_to_the_data( line, data_labels )
    
    line_scan_file.close()

# calculate background as the mean of the first and last
# 5 intensities
def get_background_for_label( label ):
    
    intensity_sum = the_data[ label ][ 0 ] + the_data[ label ][ 1 ] + the_data[ label ][ 2 ] + the_data[ label ][ 3 ] + the_data[ label ][ 4 ]
    
    label_data_length = len( the_data[ label ] )
    intensity_sum = intensity_sum + the_data[ label ][ label_data_length - 1 ]
    intensity_sum = intensity_sum + the_data[ label ][ label_data_length - 2 ]
    intensity_sum = intensity_sum + the_data[ label ][ label_data_length - 3 ]
    intensity_sum = intensity_sum + the_data[ label ][ label_data_length - 4 ]
    intensity_sum = intensity_sum + the_data[ label ][ label_data_length - 5 ]

    return intensity_sum / 10.0

# calculate total intensity by just summing all intensities
# in the line scan
def get_total_intensity_for_label( label, background ):

    intensity_sum = 0.0

    for intensity in the_data[ label ]:
        intensity_sum = intensity_sum + ( intensity - background )

    return intensity_sum


def print_data_for_label( label ):
    
    background = get_background_for_label( label )
    intensity_sum = get_total_intensity_for_label( label, background )

    return label + "\t" + str( len( the_data[ label ] ) ) + "\t" + str(background) + "\t" + str(intensity_sum)


# Run the program!

data_file = sys.argv[ 1 ]

print "Running line-scan-summer.py on " + data_file

if not os.path.isfile(data_file):
    print "Could not find file: " + data_file
    sys.exit( 1 )

read_in_the_data( data_file )

file = open("line_scan_sums.txt","w") 
 
file.write( 'line\tcount\tbackground\tintensity sum\n' )

for label in the_data:
    file.write( print_data_for_label( label ) + "\n" )

file.close() 
